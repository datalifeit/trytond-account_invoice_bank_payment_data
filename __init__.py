# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import invoice
from . import account


def register():
    Pool.register(
        invoice.Invoice,
        account.InvoiceMoveReconciliation,
        module='account_invoice_bank_payment_data', type_='model')
    Pool.register(
        invoice.ReconciliationMoveOpen,
        module='account_invoice_bank_payment_data', type_='wizard')
    Pool.register(
        invoice.BankStatementLineOpen,
        module='account_invoice_bank_payment_data', type_='wizard',
        depends=['account_bank_statement'])
