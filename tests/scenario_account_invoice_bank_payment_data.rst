==========================================
Account Invoice Bank Payment Data Scenario
==========================================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences
    >>> now = datetime.datetime.now()
    >>> today = datetime.date.today()

Install account_invoice_bank_payment_data::

    >>> config = activate_modules(['account_invoice_bank_payment_data',
    ...     'account_bank_statement_counterpart',
    ...     'account_payment_processing'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()
    >>> company.timezone = 'Europe/Madrid'
    >>> company.save()


Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']
    >>> cash.bank_reconcile = True
    >>> cash.save()


Get journal::

    >>> Journal = Model.get('account.journal')
    >>> journal_revenue, = Journal.find([
    ...         ('code', '=', 'REV'),
    ...         ])


Create processing accounts::

    >>> Account = Model.get('account.account')
    >>> processing = Account()
    >>> processing.name = 'Customers Processing Payments'
    >>> processing.parent = receivable.parent
    >>> processing.type = receivable.type
    >>> processing.reconcile = True
    >>> processing.party_required = True
    >>> processing.deferral = True
    >>> processing.save()


Create payment journal::

    >>> PaymentJournal = Model.get('account.payment.journal')
    >>> payment_journal = PaymentJournal(
    ...     name='Processing',
    ...     process_method='manual',
    ...     processing_journal=journal_revenue,
    ...     processing_account=processing)
    >>> payment_journal.save()


Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()


Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.supplier_taxes.append(tax)
    >>> account_category.save()


Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('40')
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products


Create Journal::

    >>> Sequence = Model.get('ir.sequence')
    >>> SequenceType = Model.get('ir.sequence.type')
    >>> sequence_type, = SequenceType.find([('name', '=', 'Account Journal')])
    >>> sequence = Sequence(name='Bank', sequence_type=sequence_type,
    ...     company=company)
    >>> sequence.save()
    >>> AccountJournal = Model.get('account.journal')
    >>> account_journal = AccountJournal(name='Statement',
    ...     type='cash',
    ...     sequence=sequence)
    >>> account_journal.save()
    >>> StatementJournal = Model.get('account.bank.statement.journal')
    >>> statement_journal = StatementJournal(name='Test',
    ...     journal=account_journal, account=cash,
    ...     )
    >>> statement_journal.save()


Create invoice::

    >>> Invoice = Model.get('account.invoice')
    >>> InvoiceLine = Model.get('account.invoice.line')
    >>> invoice = Invoice()
    >>> invoice.type = 'out'
    >>> invoice.party = party
    >>> invoice.invoice_date = today
    >>> line = invoice.lines.new()
    >>> line.product = product
    >>> line.quantity = 5
    >>> line.unit_price = Decimal('20')
    >>> invoice.save()
    >>> invoice.click('validate_invoice')
    >>> invoice.click('post')
    >>> reconcile, = [l for l in invoice.move.lines if l.account == receivable]


Create Bank Statement::

    >>> BankStatement = Model.get('account.bank.statement')
    >>> statement = BankStatement(journal=statement_journal, date=now)


Create Bank Statement Lines::

    >>> MoveLine = Model.get('account.move.line')
    >>> statement_line = statement.lines.new()
    >>> statement_line.date = now
    >>> statement_line.description = 'Statement Line'
    >>> statement_line.amount = Decimal('100.0')
    >>> statement.click('confirm')
    >>> statement_line, = statement.lines
    >>> reconcile = MoveLine(reconcile.id)
    >>> statement_line.counterpart_lines.append(reconcile)
    >>> statement.save()
    >>> statement_line, = statement.lines
    >>> statement_line.click('post')
    >>> bank_move_line = statement_line.bank_lines[0].move_line
    >>> invoice.reload()
    >>> invoice.state
    'paid'
    >>> invoice.reconciliation_state
    'done'


Check invoice payment move lines::

    >>> PaymentMoveLine = Model.get('account.invoice_move.reconciliation')
    >>> pay_line, = PaymentMoveLine.find([])
    >>> pay_line.invoice == invoice
    True
    >>> pay_line.type
    'receivable'
    >>> pay_line.invoice_date == today
    True
    >>> pay_line.invoice_move_line == reconcile
    True
    >>> pay_line.move_line == bank_move_line
    True
    >>> pay_line.move == bank_move_line.move
    True
    >>> pay_line.party == invoice.party
    True
    >>> pay_line.state
    'done'
    >>> pay_line.invoice_move_amount
    Decimal('100.00')
    >>> pay_line.amount
    Decimal('100.00')
    >>> pay_line.amount_to_pay
    Decimal('0')


Check invoice::

    >>> invoice.bank_payment_date == str(today)
    True
    >>> open_statement_line = Wizard('account.invoice.open_bank_statement_line', [invoice])
    >>> len(open_statement_line.actions[0])
    1


Create another invoice::

    >>> invoice2 = Invoice()
    >>> invoice2.type = 'out'
    >>> invoice2.party = party
    >>> invoice2.invoice_date = today
    >>> line = invoice2.lines.new()
    >>> line.product = product
    >>> line.quantity = 10
    >>> line.unit_price = Decimal('20')
    >>> invoice2.save()
    >>> invoice2.click('validate_invoice')
    >>> invoice2.click('post')


Process pay::

    >>> Payment = Model.get('account.payment')
    >>> line, = [l for l in invoice2.move.lines if l.account == receivable]
    >>> pay_line = Wizard('account.move.line.pay', [line])
    >>> pay_line.form.journal = payment_journal
    >>> pay_line.execute('start')
    >>> payment, = Payment.find([('state', '=', 'draft')])
    >>> payment.amount
    Decimal('200.00')
    >>> payment.click('approve')
    >>> process_payment = Wizard('account.payment.process', [payment])
    >>> process_payment.execute('process')
    >>> payment.reload()



Check invoice payment move lines::

    >>> line_processing, = MoveLine.find([('account', '=', processing)])
    >>> len(PaymentMoveLine.find([]))
    2
    >>> with config.set_context(invoice_ids=[invoice2.id]):
    ...     pay_line, = PaymentMoveLine.find([])
    >>> pay_line.invoice == invoice2
    True
    >>> pay_line.invoice_move_line == line
    True
    >>> pay_line.move_line == line_processing
    True
    >>> pay_line.move == line_processing.move
    True
    >>> pay_line.state
    'pending'
    >>> invoice2.reload()
    >>> invoice2.state
    'paid'
    >>> invoice2.state
    'pending'


Search invoices by payment state::

    >>> len(Invoice.find([('reconciliation_state', '=', 'pending')]))
    1
    >>> len(Invoice.find([('reconciliation_state', '=', 'done')]))
    1


Create Bank Statement Line::

    >>> statement_line2 = statement.lines.new()
    >>> statement_line2.date = now
    >>> statement_line2.description = 'Statement Line 2'
    >>> statement_line2.amount = Decimal('200.0')
    >>> statement_line2.party = party
    >>> statement.click('confirm')
    >>> statement_line2, = [l for l in statement.lines if l.amount == Decimal('200.0')]
    >>> statement_line2.counterpart_lines.append(line_processing)
    >>> statement.save()
    >>> statement_line2, = [l for l in statement.lines if l.amount == Decimal('200.0')]
    >>> statement_line2.click('post')
    >>> bank_move_line2 = statement_line2.bank_lines[0].move_line
    >>> invoice2.reload()
    >>> invoice2.state
    'paid'
    >>> invoice2.reconciliation_state
    'done'
    >>> with config.set_context(invoice_ids=[invoice2.id]):
    ...     pay_line, = PaymentMoveLine.find([])
    >>> pay_line.move_line == bank_move_line2
    True
    >>> pay_line.move == bank_move_line2.move
    True
    >>> pay_line.state
    'done'


Search invoices by payment state::

    >>> len(Invoice.find([('reconciliation_state', '=', 'pending')]))
    0
    >>> len(Invoice.find([('reconciliation_state', '=', 'done')]))
    2