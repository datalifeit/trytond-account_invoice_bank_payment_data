# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval, PYSONEncoder
from trytond.wizard import Wizard, StateAction
from trytond.transaction import Transaction
from itertools import groupby
from sql import Null
from sql.aggregate import Min
from sql.conditionals import Case


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    bank_payment_date = fields.Function(fields.Date('Bank Payment date'),
        'get_bank_payment_data')
    bank_amount_to_pay = fields.Function(fields.Numeric('Bank Amount to Pay',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']
        ), 'get_bank_payment_data')
    reconciliation_state = fields.Function(
        fields.Selection([
            ('none', 'None'),
            ('pending', 'Pending'),
            ('done', 'Done')], 'Reconciliation state'),
        'get_reconciliation_state', searcher='search_reconciliation_state')

    @classmethod
    def get_bank_payment_data(cls, records, names):
        pool = Pool()
        PaymentMoveLine = pool.get('account.invoice_move.reconciliation')

        record_ids = {r.id: None for r in records}
        res = {n: record_ids.copy() for n in names}

        with Transaction().set_context(invoice_ids=list(map(int, records))):
            pay_lines = PaymentMoveLine.search([])
        pay_lines = sorted(pay_lines, key=lambda x: x.invoice)
        for invoice, grouped_pay_lines in groupby(pay_lines,
                key=lambda x: x.invoice):
            grouped_pay_lines = list(grouped_pay_lines)
            if 'bank_amount_to_pay' in names:
                res['bank_amount_to_pay'][invoice.id] = sum(
                    pl.amount_to_pay for pl in grouped_pay_lines)
            elif 'bank_payment_date' in names:
                res['bank_payment_date'][invoice.id] = max(
                    pl.payment_date for pl in grouped_pay_lines)
        return res

    @classmethod
    def get_reconciliation_state_query(cls, record_ids=[]):
        pool = Pool()
        PaymentMoveLine = pool.get('account.invoice_move.reconciliation')

        priorities = {
            0: 'none',
            1: 'pending',
            2: 'done',
        }

        with Transaction().set_context(invoice_ids=record_ids):
            invoice = cls.__table__()
            pay_line = PaymentMoveLine.__table__()
            conditions = [(pay_line.state == value, key)
                for key, value in priorities.items()]
            query = invoice.join(
                pay_line,
                condition=(invoice.id == pay_line.invoice)
            ).select(
                invoice.id,
                Min(Case(*conditions, _else=0)).as_('state'),
                order_by=invoice.id,
                group_by=invoice.id
            )
        return query, priorities

    @classmethod
    def get_reconciliation_state(cls, records, name):
        cursor = Transaction().connection.cursor()
        query, priorities = cls.get_reconciliation_state_query(
            list(map(int, records)))
        cursor.execute(*query)
        res = {r.id: 'none' for r in records}
        res.update({
            x[0]: priorities[x[1]] for x in cursor.fetchall()})
        return res

    @classmethod
    def search_reconciliation_state(cls, name, clause):
        sql_table = cls.__table__()
        _, operator, value = clause
        Operator = fields.SQL_OPERATORS[operator]

        query, priorities = cls.get_reconciliation_state_query()
        conditions = [(query.state == key, value)
            for key, value in priorities.items()]
        query = sql_table.join(query, 'LEFT', condition=(
                query.id == sql_table.id)
            ).select(
                sql_table.id,
                where=Operator(Case(
                    (query.id == Null, 'none'),
                    else_=Case(*conditions, _else='none')), clause[2])
        )
        return [('id', 'in', query)]


class ReconciliationMoveOpen(Wizard):
    """Account Invoice Move Reconciliation Open"""
    __name__ = 'account.invoice_move.reconciliation.open'
    start_state = 'open_'

    open_ = StateAction(
        'account_invoice_bank_payment_data.act_invoice_payment_move_line')

    def do_open_(self, action):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceReconciliation = pool.get('account.invoice_move.reconciliation')

        invoices = Invoice.browse(Transaction().context['active_ids'])
        with Transaction().set_context(
                invoice_ids=Transaction().context['active_ids']):
            lines = [l.move_line for l in InvoiceReconciliation.search([])]
        # show full move to get all invoices reconciled with given invoices
        lines = InvoiceReconciliation.search([
            ('move_line', 'in', list(set(lines)))
        ])
        action['name'] += ' - %s' % ','.join([
            i.number for i in invoices if i.number])
        action['pyson_domain'] = PYSONEncoder().encode(
            [('id', 'in', list(map(int, lines)))])
        return action, {}


class BankStatementLineOpen(Wizard):
    """Bank Statement Line Open"""
    __name__ = 'account.invoice.open_bank_statement_line'
    start_state = 'open_'

    open_ = StateAction(
        'account_invoice_bank_payment_data.act_account_bank_statement_line')

    def do_open_(self, action):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoicePaymentMoveLine = pool.get('account.invoice_move.reconciliation')
        StatementLine = pool.get('account.bank.statement.line')

        invoices = Invoice.browse(Transaction().context['active_ids'])
        with Transaction().set_context(
                invoice_ids=Transaction().context['active_ids']):
            lines = [l.move_line for l in InvoicePaymentMoveLine.search([])]
        statement_lines = StatementLine.search([
            ('bank_lines.move_line', 'in', list(set(lines)))
        ])
        action['name'] += ' - %s' % ','.join([
            i.number for i in invoices if i.number])
        action['pyson_domain'] = PYSONEncoder().encode(
            [('id', 'in', list(map(int, statement_lines)))])
        return action, {}
