# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.tools import reduce_ids
from trytond.transaction import Transaction
from sql import With, Null, Literal, Cast, Union
from sql.conditionals import Case, Coalesce, Greatest, Least
from sql.operators import Concat
from sql.functions import CurrentDate, Substring, Position, Mod, Abs


class InvoiceMoveReconciliation(ModelSQL, ModelView):
    """Account Invoice Move Reconciliation"""
    __name__ = 'account.invoice_move.reconciliation'

    company = fields.Many2One('company.company', 'Company')
    type = fields.Selection([
        ('payable', 'Payable'),
        ('receivable', 'Receivable')], 'Type')
    invoice = fields.Many2One('account.invoice', 'Invoice')
    invoice_move = fields.Many2One('account.move', 'Invoice Move')
    invoice_move_line = fields.Many2One('account.move.line',
        'Invoice Move line')
    party = fields.Many2One('party.party', 'Party')
    invoice_date = fields.Date('Invoice date')
    payment_term = fields.Many2One('account.invoice.payment_term',
        'Payment Term')
    move_line = fields.Many2One('account.move.line', 'Move Line')
    move = fields.Many2One('account.move', 'Move')
    move_origin = fields.Reference('Move Origin', selection='get_move_origin')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('done', 'Done')], 'State')
    date = fields.Date('Date')
    invoice_move_debit = fields.Numeric('Invoice move debit',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    invoice_move_credit = fields.Numeric('Invoice move credit',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    debit = fields.Numeric('Debit',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    credit = fields.Numeric('Credit',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    currency_digits = fields.Integer('Currency digits')
    maturity_date = fields.Date('Maturity Date')
    delay = fields.TimeDelta('Delay')
    current_account = fields.Many2One('account.account', 'Current account')
    invoice_move_amount = fields.Numeric('Invoice move Amount',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    amount = fields.Numeric('Amount',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    remaining_amount = fields.Numeric('Remaining Amount',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])

    @classmethod
    def table_query(cls):
        pool = Pool()
        Account = pool.get('account.account')
        AccountType = pool.get('account.account.type')
        Company = pool.get('company.company')
        Currency = pool.get('currency.currency')
        Invoice = pool.get('account.invoice')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Payment = pool.get('account.payment')
        Reconciliation = pool.get('account.move.reconciliation')

        pay_lines = With(recursive=True)
        # non-recursive term
        account = Account.__table__()
        account_type = AccountType.__table__()
        invoice = Invoice.__table__()
        move = Move.__table__()
        move_line = MoveLine.__table__()

        non_recursive_where = (
            ((invoice.type == 'out') & (account_type.receivable))
            | ((invoice.type == 'in') & (account_type.payable))
        )
        if Transaction().context.get('invoice_ids'):
            non_recursive_where &= (
                reduce_ids(invoice.id, Transaction().context['invoice_ids'])
            )
        else:
            non_recursive_where &= move.origin.like('account.invoice,%')
        pay_lines.query = move_line.join(
                move, condition=(move.id == move_line.move)
            ).join(account, condition=(account.id == move_line.account)
            ).join(account_type, condition=(account_type.id == account.type)
            ).join(invoice, condition=(invoice.move == move.id)
            ).select(
                Cast(
                    Substring(move.origin,
                        Position(',', move.origin) + Literal(1)),
                    cls.invoice.sql_type().base).as_('invoice'),
                move_line.id.as_('invoice_move_line'),
                move_line.id.as_('move_line'),
                move.id.as_('move'),
                move_line.reconciliation,
                Case((account_type.payable, 'payable'),
                    else_=Case((account_type.receivable, 'receivable'),
                        else_='other')
                    ).as_('account_kind'),
                move_line.debit,
                move_line.credit,
                move_line.account,
                move_line.maturity_date,
                where=non_recursive_where
        )

        # recursive term
        raccount = Account.__table__()
        raccount_type = AccountType.__table__()
        rmove = Move.__table__()
        rmove_line = MoveLine.__table__()
        rmove2 = Move.__table__()
        rmove_line2 = MoveLine.__table__()
        payment = Payment.__table__()
        pay_lines.query |= rmove_line.join(rmove, condition=(
                rmove.id == rmove_line.move)
            ).join(pay_lines, condition=(
                (pay_lines.reconciliation == rmove_line.reconciliation)
                # use greater ids as normally it is created after invoice move
                & (rmove_line.id > pay_lines.move_line)
                & (rmove.id != pay_lines.move)
                & ~Coalesce(rmove.origin, '').like('account.invoice,%'))
            ).join(rmove_line2, condition=(
                (rmove_line2.move == rmove.id)
                & (Coalesce(rmove_line2.reconciliation, 0)
                    != rmove_line.reconciliation))
            ).join(payment, 'LEFT', condition=(
                payment.line == pay_lines.move_line)
            ).join(rmove2, 'LEFT', condition=(
                (rmove2.id == rmove_line2.move)
                & (Coalesce(rmove2.origin, '') == Case((payment.id != Null,
                    Concat('account.payment,', payment.id)),
                    else_=Coalesce(rmove2.origin, ''))))
            ).join(raccount, condition=(raccount.id == rmove_line2.account)
            ).join(raccount_type, condition=(
                raccount_type.id == raccount.type)
            ).select(
                pay_lines.invoice,
                pay_lines.invoice_move_line,
                rmove_line2.id,
                rmove_line2.move,
                rmove_line2.reconciliation,
                Case((raccount_type.payable, 'payable'),
                    else_=Case((raccount_type.receivable, 'receivable'),
                        else_='other')
                    ).as_('account_kind'),
                rmove_line2.debit,
                rmove_line2.credit,
                rmove_line2.account,
                Coalesce(rmove_line2.maturity_date, pay_lines.maturity_date)
        )

        # final query
        company = Company.__table__()
        currency = Currency.__table__()
        finvoice = Invoice.__table__()
        fmove = Move.__table__()
        fmove_line = MoveLine.__table__()
        where = (
            (
                (pay_lines.reconciliation == Null)
                | ~pay_lines.account_kind.in_(['payable', 'receivable'])
            )
        )

        def compose_column_id(column1, column2):
            # Pairing function from http://szudzik.com/ElegantPairing.pdf
            return Mod(
                (
                    (column1.cast('bigint') * column1.cast('bigint'))
                    + (Literal(3) * column1.cast('bigint'))
                    + (Literal(2) * column1.cast('bigint')
                        * column2.cast('bigint'))
                    + column2
                    + (column2.cast('bigint') * column2.cast('bigint'))
                    ) / Literal(2), 2147483647
                ).cast(cls.id.sql_type().base).as_('id')

        if Transaction().context.get('invoice_ids'):
            where &= (reduce_ids(finvoice.id,
                Transaction().context['invoice_ids']))
        main_query = pay_lines.join(finvoice, condition=(
                finvoice.id == pay_lines.invoice)
            ).join(company, condition=(
                company.id == finvoice.company)
            ).join(currency, condition=(currency.id == company.currency)
            ).join(fmove_line, condition=(
                fmove_line.id == pay_lines.invoice_move_line)
            ).join(fmove, condition=(
                fmove.id == pay_lines.move)
            ).select(
                compose_column_id(pay_lines.invoice_move_line,
                    pay_lines.move_line),
                fmove.create_date,
                fmove.write_date,
                fmove.create_uid,
                fmove.write_uid,
                pay_lines.invoice,
                Case((finvoice.type == 'out', 'receivable'),
                    else_='payable').as_('type'),
                finvoice.company,
                finvoice.party,
                finvoice.invoice_date,
                finvoice.payment_term,
                pay_lines.maturity_date,
                pay_lines.invoice_move_line,
                fmove_line.move.as_('invoice_move'),
                fmove_line.debit.as_('invoice_move_debit'),
                fmove_line.credit.as_('invoice_move_credit'),
                (fmove_line.debit - fmove_line.credit
                    ).as_('invoice_move_amount'),
                pay_lines.move_line,
                pay_lines.move,
                fmove.origin.as_('move_origin'),
                pay_lines.debit,
                pay_lines.credit,
                Case((pay_lines.account_kind.in_(['payable', 'receivable']),
                    Literal(Null)), else_=fmove.date).as_('date'),
                Case((pay_lines.maturity_date != Null,
                    (Case(
                        (pay_lines.account_kind.in_(['payable', 'receivable']),
                            CurrentDate()), else_=fmove.date)
                    - pay_lines.maturity_date).cast('bigint') * 60 * 60 * 24),
                    else_=Null).as_('delay'),
                Case((pay_lines.account_kind.in_(['payable', 'receivable']),
                    'pending'), else_='done').as_('state'),
                (pay_lines.credit - pay_lines.debit).as_('amount'),
                Case((finvoice.type == 'out',
                    Greatest(
                        (fmove_line.debit - fmove_line.credit)
                        + (pay_lines.credit - pay_lines.debit), 0)),
                    else_=Least(
                        (fmove_line.debit - fmove_line.credit)
                        + (pay_lines.credit - pay_lines.debit), 0)
                    ).as_('remaining_amount'),
                pay_lines.account.as_('current_account'),
                currency.digits.as_('currency_digits'),
                where=where,
                with_=[pay_lines]
            )

        # invoices reconcililed among them
        account = Account.__table__()
        icompany = Company.__table__()
        icurrency = Currency.__table__()
        iinvoice = Invoice.__table__()
        imove = Move.__table__()
        imove_line = MoveLine.__table__()
        rimove = Move.__table__()
        rimove_line = MoveLine.__table__()
        reconciliation = Reconciliation.__table__()

        credit_invoices_where = (
            rimove.origin.like('account.invoice,%')
            # discard compensation move which copy origin
            & (Coalesce(rimove.origin, '') != Coalesce(imove.origin, '')))
        if Transaction().context.get('invoice_ids'):
            credit_invoices_where &= (
                reduce_ids(iinvoice.id, Transaction().context['invoice_ids'])
            )
        else:
            credit_invoices_where &= imove.origin.like('account.invoice,%')
        credit_invoices_query = imove_line.join(
                imove, condition=(imove.id == imove_line.move)
            ).join(iinvoice, condition=(iinvoice.move == imove.id)
            ).join(reconciliation, condition=(
                reconciliation.id == imove_line.reconciliation)
            ).join(rimove_line, condition=(
                (rimove_line.reconciliation == reconciliation.id)
                & (rimove_line.move != imove.id)
                & (rimove_line.credit == imove_line.debit)
                & (rimove_line.debit == imove_line.credit))
            ).join(rimove, condition=(rimove.id == rimove_line.move)
            ).join(icompany, condition=(
                icompany.id == iinvoice.company)
            ).join(icurrency, condition=(icurrency.id == icompany.currency)
            ).select(
                compose_column_id(imove_line.id, rimove_line.id),
                imove.create_date,
                imove.write_date,
                imove.create_uid,
                imove.write_uid,
                iinvoice.id.as_('invoice'),
                Case((iinvoice.type == 'out', 'receivable'),
                    else_='payable').as_('type'),
                iinvoice.company,
                iinvoice.party,
                iinvoice.invoice_date,
                iinvoice.payment_term,
                imove_line.maturity_date,
                imove_line.id.as_('invoice_move_line'),
                imove.id.as_('invoice_move'),
                imove_line.debit.as_('invoice_move_debit'),
                imove_line.credit.as_('invoice_move_credit'),
                (imove_line.debit - imove_line.credit
                    ).as_('invoice_move_amount'),
                rimove_line.id.as_('move_line'),
                rimove.id.as_('move'),
                rimove.origin.as_('move_origin'),
                rimove_line.debit,
                rimove_line.credit,
                Greatest(rimove.date, imove.date).as_('date'),
                Literal(Null).as_('delay'),
                Literal('done').as_('state'),
                (rimove_line.credit - rimove_line.debit).as_('amount'),
                Literal(0).as_('remaining_amount'),
                rimove_line.account.as_('current_account'),
                icurrency.digits.as_('currency_digits'),
                where=credit_invoices_where
        )
        return Union(main_query, credit_invoices_query, all_=True)

    @classmethod
    def get_move_origin(cls):
        return Pool().get('account.move').get_origin()
