datalife_account_invoice_bank_payment_data
==========================================

The account_invoice_bank_payment_data module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_invoice_bank_payment_data/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_invoice_bank_payment_data)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
